# ppro-kafka-connector

requirements:

1. docker run -p 3306:3306 mariadb
2. create testDB
3. run kafka broker:
    - bin/zookeeper-server-start.sh config/zookeeper.properties
    - bin/kafka-server-start.sh config/server.properties
4. run application in intellij
5. INSERT INTO `testDB`.`t_app_users` (`id`, `password`, `username`) VALUES ('1', 'test', 'franz');

