package deeplab.connector.deeplabconnector.kafka;

import java.util.List;


import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long>{
    List<Message> findByText(String username);
    List<Message> getAllByStatus(String status);
    List<Message> findAll();
}
