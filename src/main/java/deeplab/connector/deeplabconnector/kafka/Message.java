package deeplab.connector.deeplabconnector.kafka;


import javax.persistence.*;

@Entity
@Table(name = "t_app_messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "timestamp")
    private String timestamp;
    @Column(name = "status")
    private String status;
    @Column(name = "text")
    private String text;

    public Message() {}

    public Message(String timestamp, String text) {
        this.timestamp = timestamp;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", timestamp='" + timestamp + '\'' +
                ", status='" + status + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
