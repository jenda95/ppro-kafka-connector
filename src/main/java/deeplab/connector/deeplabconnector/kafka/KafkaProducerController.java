package deeplab.connector.deeplabconnector.kafka;

import deeplab.connector.deeplabconnector.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaProducerController
{
    private final KafkaProducerService producerService;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    public KafkaProducerController(KafkaProducerService producerService, MessageRepository messageRepository)
    {
        this.producerService = producerService;
        this.messageRepository = messageRepository;
        Message messageObj = new Message();
        String timestamp = Util.getTimestampNow();
        messageObj.setTimestamp(timestamp);
        messageObj.setText("test");
        messageObj.setStatus("rec");

        messageObj.setTimestamp(timestamp);
        messageObj.setText("test");
        messageObj.setStatus("sent");
        this.messageRepository.save(messageObj);
    }

    @PostMapping(value = "/publish")
    public ModelAndView sendMessageToKafkaTopic(@RequestParam("message") String message, ModelMap modelMap)
    {
        this.producerService.sendMessage(message);
        Message messageObj = new Message();
        String timestamp = Util.getTimestampNow();
        messageObj.setTimestamp(timestamp);
        messageObj.setText(message);
        messageObj.setStatus("sent");
        messageRepository.save(messageObj);
        return new ModelAndView("redirect:/messagestable",  modelMap);
    }

}