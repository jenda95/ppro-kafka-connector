package deeplab.connector.deeplabconnector.kafka;
import deeplab.connector.deeplabconnector.database.UserRepository;
import deeplab.connector.deeplabconnector.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


@Service
public class KafkaConsumerService
{
    @Autowired
    MessageRepository repository;

    private static final String TOPIC = "test";
    private static final String GROUP_ID = "gid";

    private final Logger logger =
            LoggerFactory.getLogger(KafkaConsumerService.class);

    @KafkaListener(topics = TOPIC,
            groupId = GROUP_ID)
    public void consume(String message)
    {
        logger.info(String.format("Message recieved -> %s", message));
        Message messageObj = new Message();
        String timestamp = Util.getTimestampNow();
        messageObj.setTimestamp(timestamp);
        messageObj.setStatus("rec");
        messageObj.setText(message);
        repository.save(messageObj);
    }
}