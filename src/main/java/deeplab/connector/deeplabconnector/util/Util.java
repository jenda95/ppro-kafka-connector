package deeplab.connector.deeplabconnector.util;

import deeplab.connector.deeplabconnector.kafka.Message;
import deeplab.connector.deeplabconnector.kafka.MessageStats;
import deeplab.connector.deeplabconnector.kafka.config.ConfigAttribute;

import java.util.*;

public class Util {

    public static List<ConfigAttribute> parseConfigToConfigAttributes(String[] stringToParse, String delimiter) {
        List<ConfigAttribute> confList = new ArrayList<>();
        for(int i = 0; i < stringToParse.length; i++) {
            ConfigAttribute configAttribute = new ConfigAttribute();
            String out[] = stringToParse[i].split(delimiter);
            configAttribute.setKey(out[0]);
            configAttribute.setValue(out[1]);
            confList.add(configAttribute);
        }
        return confList;
    }

    public static List<Map<String, String>> parseConfig(String[] stringToParse, String delimiter) {
        List<Map<String, String>> confList = new ArrayList<>();
        for(int i = 0; i < stringToParse.length; i++) {
            Map <String, String> confMap = new HashMap<>();
            String out[] = stringToParse[i].split(delimiter);
            confMap.put("key", out[0]);
            confMap.put("value", out[1]);
            confList.add(confMap);
        }
        return confList;
    }

    public static String[] parseConfigToList(String stringToParse, String delimiter) {
        return stringToParse.split(delimiter);
    }

    public static String getTimestampNow() {
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
        return currentTimestamp.toString();
    }

    public static List<MessageStats> getLengthList(List<Message> messageList) {
        List<MessageStats> messageStatsList = new ArrayList<>();
        for(int i = 0; i < messageList.size(); i++) {
            Message message = messageList.get(i);
            MessageStats messageStats = new MessageStats(message.getId(), message.getText().length());
            messageStatsList.add(messageStats);
        }
        return messageStatsList;
    }

}
