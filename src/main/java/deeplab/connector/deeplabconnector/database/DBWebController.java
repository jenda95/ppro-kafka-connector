package deeplab.connector.deeplabconnector.database;

import java.util.ArrayList;
import java.util.List;
import deeplab.connector.deeplabconnector.web.StatusMessage;
import deeplab.connector.deeplabconnector.web.login.LoginForm;
import deeplab.connector.deeplabconnector.web.user.UserUpdateForm;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarActiveLink;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DBWebController {

    @Autowired
    private UserRepository repository;

//    @GetMapping("/findall")
//    public String findAll() {
//
//        String result = "";
//
//        for (User user : repository.findAll()) {
//            result += user + "</br>";
//        }
//
//        return result;
//    }

    @PostMapping("/login")
    public ModelAndView loginSubmit(@ModelAttribute LoginForm loginForm, Model model, ModelMap modelMap, HttpServletRequest request) {
        model.addAttribute("loginForm", new LoginForm());
        List <User> users = new ArrayList<>();
        if(repository.existsByUsername(loginForm.getUsername())){
            users = repository.findByUsername(loginForm.getUsername());
        }

        System.out.println(loginForm.getUsername());
        System.out.println(loginForm.getPassword());

//        Here should be use one way hash, this is big vulnerability
        if(repository.existsByUsername(loginForm.getUsername()) && loginForm.getPassword().equals(users.get(0).getPassword())) {
            //get the notes from request session
            List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
            //check if notes is present in session or not
            if (users_session == null) {
                users_session = new ArrayList<>();
                // if notes object is not present in session, set notes in the request session
                request.getSession().setAttribute("USERS_SESSION",users_session);
            }
            users_session.add(users.get(0));
            request.getSession().setAttribute("USERS_SESSION",users_session);
//            model.addAttribute("statusMessage", new StatusMessage("yep"));
            return new ModelAndView("redirect:/dashboard",  modelMap);
        } else {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage("false"));
            return new ModelAndView("redirect:/authfailed",  modelMap);
        }
    }

    @RequestMapping(value = "/user")
    public String user(HttpServletRequest request, Model model, ModelMap modelMap) {
        //get the notes from request session
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            System.out.println(users_session.get(0).getUsername());
            List < User > userList = repository.findByUsername(users_session.get(0).getUsername());
            User user = userList.get(0);
            System.out.println(user.toString());
            UserUpdateForm userUpdateForm = new UserUpdateForm();
            userUpdateForm.setUsername(user.getUsername());
            userUpdateForm.setEmail(user.getEmail());
            userUpdateForm.setFirstName(user.getFirstName());
            userUpdateForm.setLastName(user.getLastName());
            userUpdateForm.setAddress(user.getAddress());
            userUpdateForm.setCity(user.getCity());
            userUpdateForm.setCountry(user.getCountry());
            userUpdateForm.setPostalCode(user.getPostalCode());
            userUpdateForm.setAbout(user.getAbout());
            userUpdateForm.setCompany(user.getCompany());

            model.addAttribute("userUpdateForm", userUpdateForm);
            model.addAttribute("userInfo", user);
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.ORANGE));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.USER_PROFILE));
            return "/user";
        }
    }

    @PostMapping("/userUpdate")
    public ModelAndView userUpdate(@ModelAttribute UserUpdateForm userUpdateForm, ModelMap modelMap, Model model) {
        System.out.println("Username: " + userUpdateForm.getUsername());
        System.out.println("Password: " + userUpdateForm.getPassword());
        System.out.println("Email: " + userUpdateForm.getEmail());
        System.out.println("comp: " + userUpdateForm.getCompany());
        System.out.println("firstname: " + userUpdateForm.getFirstName());
        List < User > users = repository.findByUsername(userUpdateForm.getUsername());
        User user = users.get(0);
        System.out.println(users.toString());
        user.setEmail(userUpdateForm.getEmail());
        user.setFirstName(userUpdateForm.getFirstName());
        user.setLastName(userUpdateForm.getLastName());
        user.setAddress(userUpdateForm.getAddress());
        user.setCity(userUpdateForm.getCity());
        user.setCountry(userUpdateForm.getCountry());
        user.setPostalCode(userUpdateForm.getPostalCode());
        user.setAbout(userUpdateForm.getAbout());
        user.setCompany(userUpdateForm.getCompany());
        repository.save(user);
        return new ModelAndView("redirect:/user",  modelMap);
    }

}
