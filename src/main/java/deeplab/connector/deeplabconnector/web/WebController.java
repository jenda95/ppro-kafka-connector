package deeplab.connector.deeplabconnector.web;

import deeplab.connector.deeplabconnector.database.User;
import deeplab.connector.deeplabconnector.kafka.Message;
import deeplab.connector.deeplabconnector.kafka.MessageRepository;
import deeplab.connector.deeplabconnector.kafka.MessageStats;
import deeplab.connector.deeplabconnector.kafka.config.ConfigAttribute;
import deeplab.connector.deeplabconnector.kafka.config.KafkaConsumerConfig;
import deeplab.connector.deeplabconnector.util.Util;
import deeplab.connector.deeplabconnector.web.login.LoginForm;
import deeplab.connector.deeplabconnector.web.messageForms.SendMessageForm;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarActiveLink;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@Controller
public class WebController {

    @Autowired
    private KafkaConsumerConfig consumerConfig;

    @Autowired
    private MessageRepository messageRepository;


    @RequestMapping(value = "/index")
    public ModelAndView index(ModelMap modelMap) {
        //        model.addAttribute("attribute", "redirectWithRedirectPrefix");
        return new ModelAndView("redirect:/login", modelMap);
    }

    @RequestMapping(value = "/dashboard")
    public String dashboard(HttpServletRequest request, Model model) {
        //get the notes from request session
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            List<Message> recMessagesList = messageRepository.getAllByStatus("rec");
            List<Message> sentMessagesList = messageRepository.getAllByStatus("sent");
            List<Message> allMessages = messageRepository.findAll();
            List<MessageStats> messageStatsList = Util.getLengthList(allMessages);
            System.out.println(allMessages.toString());

            int countRec = recMessagesList.size();
            int countSent = sentMessagesList.size();

            String lastRecTimestamp = "";
            if(recMessagesList.size()>0) {
                lastRecTimestamp = recMessagesList.get(recMessagesList.size()-1).getTimestamp();
            }

            System.out.println(recMessagesList.toString());
            model.addAttribute("messageStatsList", messageStatsList);
            model.addAttribute("lastRecTimestamp", lastRecTimestamp);
            model.addAttribute("recMessagesList", recMessagesList);
            model.addAttribute("sentMessagesList", sentMessagesList);
            model.addAttribute("countRec",countRec);
            model.addAttribute("countSent", countSent);
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.PURPLE));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.DASHBOARD));
            return "dashboard";
        }
    }

    @RequestMapping(value = "/table")
    public String table(HttpServletRequest request, Model model) {
        //get the notes from request session
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            Map<String, Object> configMap = this.consumerConfig.consumerConfig();
            String[] preparsedConfig = Util.parseConfigToList(configMap.toString(), ",");
            for(int i = 0; i < preparsedConfig.length; i++) {
                System.out.println(preparsedConfig[i]);
            }
            List<ConfigAttribute> confList = Util.parseConfigToConfigAttributes(preparsedConfig, "=");
            for (ConfigAttribute configAttribute: confList
                 ) {
                System.out.println(configAttribute.getKey());
                System.out.println(configAttribute.getValue());
            }
            model.addAttribute("confList", confList);
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.BLUE));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.TABLE_LIST));
            return "table";
        }
    }

    @RequestMapping(value = "/messagestable")
    public String messagestable(HttpServletRequest request, Model model) {
        //get the notes from request session
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            List<Message> recMessagesList = messageRepository.getAllByStatus("rec");
            List<Message> sentMessagesList = messageRepository.getAllByStatus("sent");
            model.addAttribute("sendMessageForm", new SendMessageForm());
            model.addAttribute("recMessagesList", recMessagesList);
            model.addAttribute("sentMessagesList", sentMessagesList);
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.GREEN));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.MESSAGES_TABLE));
            return "messagestable";
        }
    }

    @RequestMapping(value = "/maps")
    public String maps(HttpServletRequest request, Model model) {
        //get the notes from request session
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.RED));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.MAPS));
            return "maps";
        }
    }

//    @RequestMapping(value = "/user")
//    public String user(HttpServletRequest request, Model model) {
//        //get the notes from request session
//        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
//        //check if notes is present in session or not
//        if (users_session == null) {
//            model.addAttribute("loginForm", new LoginForm());
//            model.addAttribute("statusMessage", new StatusMessage());
//            return "login";
//        } else {
////            model.addAttribute("statusMessage", new StatusMessage());
//            Object user = request.getSession().getAttribute("USER_INFO");
//            if(user != null) {
////                System.out.println(user.toString());
//            }
////            model.addAttribute("userInfo", user);
////            System.out.println(model.getAttribute("userInfo"));
//            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.ORANGE));
//            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.USER_PROFILE));
//            return "user";
//        }
//    }
}
