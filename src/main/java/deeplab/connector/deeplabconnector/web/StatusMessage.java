package deeplab.connector.deeplabconnector.web;

public class StatusMessage {
    static final String INFO = "info";
    static final String WARN = "warn";

    private String message;
    private String type;

    public StatusMessage(){};

    public StatusMessage(String message) {
        this.message = message;
    }

    public StatusMessage(String message, String type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
