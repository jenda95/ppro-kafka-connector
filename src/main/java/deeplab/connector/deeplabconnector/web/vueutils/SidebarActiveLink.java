package deeplab.connector.deeplabconnector.web.vueutils;

public class SidebarActiveLink {
    public static final String DASHBOARD = "dashboard";
    public static final String USER_PROFILE = "user";
    public static final String TABLE_LIST = "table";
    public static final String MESSAGES_TABLE = "messagestable";
    public static final String TYPOGRAPHY = "typo";
    public static final String ICONS = "icons";
    public static final String MAPS = "maps";
    public static final String NOTIFICATIONS = "notifications";

    private String activeLink;

    public SidebarActiveLink(String activeLink) {
        this.activeLink = activeLink;
    }

    public String getActiveLink() {
        return activeLink;
    }

    public void setActiveLink(String activeLink) {
        this.activeLink = activeLink;
    }
}
