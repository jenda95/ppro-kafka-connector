package deeplab.connector.deeplabconnector.web.vueutils;

public class SidebarColor {
//    you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
    public static final String BLUE = "blue";
    public static final String AZURE = "azure";
    public static final String GREEN = "green";
    public static final String ORANGE = "orange";
    public static final String RED = "red";
    public static final String PURPLE = "purple";

    private String color;

    public SidebarColor(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
