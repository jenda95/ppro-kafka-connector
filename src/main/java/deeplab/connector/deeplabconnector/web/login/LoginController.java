package deeplab.connector.deeplabconnector.web.login;

import deeplab.connector.deeplabconnector.database.User;
import deeplab.connector.deeplabconnector.kafka.Message;
import deeplab.connector.deeplabconnector.kafka.MessageRepository;
import deeplab.connector.deeplabconnector.kafka.MessageStats;
import deeplab.connector.deeplabconnector.util.Util;
import deeplab.connector.deeplabconnector.web.StatusMessage;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarActiveLink;
import deeplab.connector.deeplabconnector.web.vueutils.SidebarColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class LoginController {

    @Autowired
    private MessageRepository messageRepository;


    @RequestMapping(value = "/login")
    public String login(Model model, HttpServletRequest request) {
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.PURPLE));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.DASHBOARD));
            return "dashboard";
        }

    }

    @RequestMapping(value = "/")
    public String slashEmpty(ModelMap model, HttpServletRequest request) {
//        model.addAttribute("attribute", "redirectWithRedirectPrefix");
        List<User> users_session = (List<User>) request.getSession().getAttribute("USERS_SESSION");
        //check if notes is present in session or not
        if (users_session == null) {
            model.addAttribute("loginForm", new LoginForm());
            model.addAttribute("statusMessage", new StatusMessage());
            return "login";
        } else {
            List<Message> recMessagesList = messageRepository.getAllByStatus("rec");
            List<Message> sentMessagesList = messageRepository.getAllByStatus("sent");
            List<Message> allMessages = messageRepository.findAll();
            List<MessageStats> messageStatsList = Util.getLengthList(allMessages);
            System.out.println(allMessages.toString());

            int countRec = recMessagesList.size();
            int countSent = sentMessagesList.size();

            String lastRecTimestamp = "";
            if(recMessagesList.size()>0) {
                lastRecTimestamp = recMessagesList.get(recMessagesList.size()-1).getTimestamp();
            }



            System.out.println(recMessagesList.toString());
            model.addAttribute("messageStatsList", messageStatsList);
            model.addAttribute("lastRecTimestamp", lastRecTimestamp);
            model.addAttribute("recMessagesList", recMessagesList);
            model.addAttribute("sentMessagesList", sentMessagesList);
            model.addAttribute("countRec",countRec);
            model.addAttribute("countSent", countSent);

            model.addAttribute("sidebarColor", new SidebarColor(SidebarColor.PURPLE));
            model.addAttribute("active", new SidebarActiveLink(SidebarActiveLink.DASHBOARD));
            return "dashboard";
        }
    }

    @PostMapping("/logout")
    public ModelAndView logout(ModelMap model, ModelMap modelMap, HttpServletRequest request) {
        //invalidate the session , this will clear the data from configured database (Mysql/redis/hazelcast)
        request.getSession().invalidate();
        return new ModelAndView("redirect:/login",  modelMap);
    }


}
