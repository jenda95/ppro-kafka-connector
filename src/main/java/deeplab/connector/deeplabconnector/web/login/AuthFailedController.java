package deeplab.connector.deeplabconnector.web.login;

import deeplab.connector.deeplabconnector.web.StatusMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AuthFailedController {

    @RequestMapping(value = "/authfailed")
    public String login(Model model) {
        model.addAttribute("loginForm", new LoginForm());
        model.addAttribute("statusMessage", new StatusMessage("message"));
        return "login";
    }
}
