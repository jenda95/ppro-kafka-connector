package deeplab.connector.deeplabconnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DeeplabConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeeplabConnectorApplication.class, args);
	}
}
